var express = require ('express'), 					 	// traemos a express
	app 	= express(),								// llamamos al metodo y lo guardamos en app
	server  = require ('http').createServer(app),		// creamos el servidor de express
	io 		= require('socket.io').listen(server);		// llamamos al socket y lo ponemos a escuchar al servidor
var	userNum = 0;
var user_online=[];//new Array()

var fork    = require("child_process").fork;

//ya tenemos el servidor con el socket y express para facilitarnos la vista
console.log('escuchando por el puerto 3000');
server.listen(3000);

// cuando ingrese al index de la app -- dirigira o renderizara a client.html
app.get("/",function(req,res){
	// res.status(200).send("hola mundo como estan");
	res.sendfile('public/client.html')
})
 

// aqui el servidor esta esperando las conexiones de los clientes
io.sockets.on('connection', function(socket){
 
	socket.on("id",function(user){
		user=userNum++;
		user_online[user]=socket.id;

		// for (x=0;x<=user;x++){
		// 	console.log("Usuario conectado Id asignada : "+user_online[x]);
		// }				
		//----->   ----->   ---->  ---->   ---->   ---->   --->   --->
		// le Asigna a Cada Usuario que se Conecta Un id con socket.id
		socket.emit('id_session',socket.id);

		// luego transmite todos los id en el chat de user_online
		io.sockets.emit('user_online',user_online);



	});

	socket.on('end',function(user_id){
		io.sockets.emit('res_disconnect',user_id);
		
		for (x=0;x<=user_online.length;x++){
			if (user_online[x]==user_id){
				console.log("el Usuario -> "+user_online[x]+" <- se ha desconectado");
				user_online[x]=0;
			}
		}
		io.sockets.emit('user_online',user_online);
	});


	socket.on('send_mensaje',function(data){
		// en este caso Reenvio el mensaje a todos 
		io.sockets.emit('emision mensaje',data);
	});
	

	// Aqui se debe abrir el subproceso para iniciar el juego
	socket.on("retando",function(data){
		console.log("se abrira un nuevo proceso con: "+ data.retador+" Vs "+ data.retado);
		// var sp1 = fork("server.js");
		// sp1.send(data);

	})

   
	
})
